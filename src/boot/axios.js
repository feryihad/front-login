  import axios from 'axios'
  const axiosInstance = axios.create({
    baseURL: 'http://localhost/phalcon_dev/'

  })
  export default ({ Vue }) => {
    Vue.prototype.$axios = axiosInstance
  }
  export { axiosInstance }
